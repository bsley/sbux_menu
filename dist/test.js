// gather menu items
function loadMenu() {

    // scroll to top of body when new items are loaded
    $('html, body').scrollTop(0);

    // gather JSON
    $.getJSON('menu_coffee.json', function(items) {
      var output = "";
      var currentCategory = "<div>";
      var i, coffee, j, asset;
      for (i = 0; i < items.result.coffees.coffee.length; i++) {
        coffee = items.result.coffees.coffee[i];
        if (coffee.forms.form.assets) {
          if (coffee.forms.form.assets.asset) {
            for (j = 0; j < coffee.forms.form.assets.asset.length; j++) {
              asset = coffee.forms.form.assets.asset[j];
              output += "<img src='" + asset.uri + "'>'" + "\n";
            }
          }
          else {
            console.log("no .asset found on #" + i)
          }
        }
      }
      // put the menu items in the Crate
      $("body").html(output);
      //console.log(output)
    });
};

// initial state
loadMenu();



