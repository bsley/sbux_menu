Using `Bower` to manage libraries, `npm` to manage `Grunt`, and `Grunt` to compile `Stylus` and concatonate Javascript.

How can I use this?

1. Clone the repo.
2. Run `npm install` and `bower install`.
3. Run `grunt`.
4. Have fun. 

—

To do list:

Pattern Library:

- Global Type adjustments
- Tab / Nested List Nav component
- Index of MenuItems component

Ordering:

- Conditional Logic for mobile/desktop nav functionality
