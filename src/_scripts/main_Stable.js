
// gather menu items
function loadMenu(itemView) {
    // scroll to top of body when new items are loaded
    $('html, body').scrollTop(0);
    // gather JSON
    $.getJSON('menu_' + itemView + '.json', function(drinks) {
      // sory the items by category
      var getMenuItem  = drinks.menuItems;
      var sortedMenuItems = getMenuItem.sort(function(a,b){
        return a.parentCategory > b.parentCategory ?1 :-1
      });

      // establish some global variables
      var output = "";
      var currentCategory = "<div>";

      // loop through each item
      for (var i in sortedMenuItems) {
          if (drinks.menuItems[i].parentCategory != currentCategory) {
            currentCategory =  drinks.menuItems[i].parentCategory; 
            output += "</div><div class='MenuGroup' id='Menu_" + currentCategory + "'><div class='MenuGroupLabel'>" + currentCategory + "</div>";
            output += "<div class='MenuItem " + drinks.menuItems[i].globalCategory + " " + drinks.menuItems[i].parentCategory + " '><div class='MenuItemThumb' style='background-image: url(" + drinks.menuItems[i].photos.squareThumb + ")'></div><div class='MenuItemDetails'><div class='MenuItemTitle'>" + drinks.menuItems[i].name + "<span class='MenuItemCal'>" + drinks.menuItems[i].calories + "Cal</span></div></div><button class='MenuItemAddButton'></button></div>";
          }
          else if (drinks.menuItems[i].parentCategory = currentCategory) {
            output += "<div class='MenuItem " + drinks.menuItems[i].globalCategory + " " + drinks.menuItems[i].parentCategory + "'><div class='MenuItemThumb' style='background-image: url(" + drinks.menuItems[i].photos.squareThumb + ")'></div><div class='MenuItemDetails'><div class='MenuItemTitle'>" + drinks.menuItems[i].name + "<span class='MenuItemCal'>" + drinks.menuItems[i].calories + "Cal</span></div></div><button class='MenuItemAddButton'></button></div>";
          }
      }

      // put the menu items in the Crate
      document.getElementById("liveContent").innerHTML=output;

      // fade them in all nice and pretty like
      $('.MenuItem').velocity("transition.slideUpIn", {
        delay: 0,
        duration: 300,
        stagger: 50,
        display: 'inline-block'
      });
      $('.MenuGroupLabel').velocity("transition.slideUpIn", {
        delay: 0,
        duration: 300,
        stagger: 150,
        display: 'block'
      });
    });
};

// initial state
loadMenu("main");
$(".MenuNav").load("menus.html #menuNav_main");


// traverse the menu!
$('body').on('click', '.MenuItem.Main', function (){
  loadMenu("espresso");
$(".MenuNav").load("menus.html #menuNav_espresso");
});

$('body').on('click', '.BacktoMain', function (){
  loadMenu("main");
$(".MenuNav").load("menus.html #menuNav_main");
});




