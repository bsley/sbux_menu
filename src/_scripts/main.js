
//manipulate urls and history
// function renderPage(pageTitle){
//   var stateObj = { foo: pageTitle };
//   history.pushState(stateObj, "page 2", pageTitle);
// }

// gather menu items
function loadMenu(itemView, filterCategory) {

    // scroll to top of body when new items are loaded
    $('html, body').scrollTop(0);

    // gather JSON
    $.getJSON('menu_' + itemView + '.json', function(drinks) {

      // sort the menu
      var getMenuItem  = drinks.menuItems;
        var sortedMenuItems = getMenuItem.sort(function(a,b){
          return a.parentCategory > b.parentCategory ?1 :-1
      }); 
      // console.log(sortedMenuItems)
      // filter the menu
      if (filterCategory) {
        var filteredMenuItems = new Array();
        for (var i in sortedMenuItems) {
          if (drinks.menuItems[i].parentCategory === filterCategory) {
          filteredMenuItems[i] = drinks.menuItems[i];
          }
        }
      }
      else {
        console.log("no filter provided!");
        filteredMenuItems = sortedMenuItems;
      } 

      // establish some global variables
      var output = "";
      var currentCategory = "<div>";

      var loadMenuThumb = function() {
        return "<div class='MenuItemThumb' style='background-image: url(" + drinks.menuItems[i].photos.squareThumb + ")'></div>"
      };
      var loadMenuCalories = function() {
        return "<span class='MenuItemCal'>" + drinks.menuItems[i].calories + " Cal</span>"
      };
      var loadMenuAddButton = function() {
        return "<button class='MenuItemAddButton'></button>"
      };

      //populate the menu
      var loadMenuItem = function() {
        output += "<div class='MenuItem " + drinks.menuItems[i].cardTemplate + " " + drinks.menuItems[i].parentCategory + " '>";
        if(drinks.menuItems[i].hasOwnProperty('photos')){
          if(drinks.menuItems[i].photos.hasOwnProperty('squareThumb')){
            output += loadMenuThumb();
          }
        }

        // does it have calories?
        output += "<div class='MenuItemDetails'><div class='MenuItemTitle'>" + drinks.menuItems[i].name;
        if(drinks.menuItems[i].hasOwnProperty('calories')){
          output += loadMenuCalories();
        }
        output += "</div></div>";
        if(drinks.menuItems[i].hasOwnProperty('purchasable')){
          if(drinks.menuItems[i].purchasable === true) {
            output += loadMenuAddButton();
          }
        }
        output += "</div>";
      };


      // loop through each item
      for (var i in filteredMenuItems) {
          if (drinks.menuItems[i].parentCategory != currentCategory) {
            currentCategory =  drinks.menuItems[i].parentCategory; 
            // make a category label
            output += "</div><div class='MenuGroup' id='Menu_" + currentCategory + "'><div class='MenuGroupLabel'>" + currentCategory + "</div>";
            loadMenuItem();
          }
          else if (drinks.menuItems[i].parentCategory = currentCategory) {
            loadMenuItem();
          }
      }

      // put the menu items in the Crate
      document.getElementById("liveContent").innerHTML=output;

      // fade them in all nice and pretty like
      $('.MenuItem').velocity("transition.slideUpIn", {
        delay: 0,
        duration: 100,
        stagger: 40,
        display: 'inline-block'
      });
      $('.MenuGroupLabel').velocity("transition.slideUpIn", {
        delay: 0,
        duration: 300,
        stagger: 150,
        display: 'block'
      });
    });
};

function loadNav(navtoload) {
  $(".MenuNav").load("menus.html #menuNav_" + navtoload);
  if (document.documentElement.clientWidth < 800) {
    // var HeaderHeight = ($('.PrimaryCrate header').height()) + ($('.PrimaryCrate .MenuNav').height());
    // $('.SecondaryCrate').css("margin-top", (HeaderHeight + "px"));
    //$('.SecondaryCrate').css("margin-top", "189px")
  }
};

function closeMenuDetail() {
  $('.MenuDetail').velocity("transition.slideOutIn", {
    duration: 300,
  });
  $('.SecondaryCrate, .PrimaryCrate').removeClass("InActive");
  $('.MenuDetail').removeClass("Active");
  window.setTimeout( function() {
    $("#MenuDetailCont").html("");
  }, 300);

};


// initial state
loadMenu("main");
loadNav("main");

// tap an item!
$('body').on('click', '.MenuItem.Drinks', function (){
  loadMenu("espresso");
  loadNav("espresso");
  renderPage("espresso");
});

// go back to the main menu!
$('body').on('click', '#menuLinkMain', function (){
  loadMenu("main");
  loadNav("main");
  renderPage("");
});

// see subcategories
$('body').on('click', '#menuLinkSubcategories', function (){
  loadMenu("subcategories");
});

// see subcategories
$('body').on('click', '#menuLinkEspresso', function (){
  loadMenu("espresso");
});

// go back to the main menu!
$('body').on('click', '#menuLinkFeatured', function (){
  loadMenu("featured");
  loadNav("featured");
});

// see subcategory filtered!
$('body').on('click', '#SubcatMain li', function (){
  closeMenuDetail();
  var visibleCategory = $(this).attr("id");
  console.log(visibleCategory);
  loadMenu("main", visibleCategory);
});

$('body').on('click', '#SubcatEspresso li', function (){
  closeMenuDetail();
  var visibleCategory = $(this).attr("id");
  console.log(visibleCategory);
  loadMenu("espresso", visibleCategory);
});

// see item detail
$('body').on('click', '.PhotoWithText', function (){
  var bg = $(this).find(".MenuItemThumb").css('background-image');
  var bg = bg.replace('url(','').replace(')','');
  console.log(bg);
  $('#MenuDetailCont').load('menu_detail.html', function() {
        window.setTimeout( function() {
          $('.MenuDetail').addClass("Active");
          $('.MenuDetailThumb').css({'background-image': 'url(' + bg + ')',});
        }, 50);
        $('.SecondaryCrate').addClass("InActive");
        if (document.documentElement.clientWidth < 480) {
          $('.PrimaryCrate').addClass("InActive");
        }
    });

});


// close menu detail
$('body').on('click', '.MenuDetailClose, .PrimaryCrate, #menuLinkMain', function (){
  closeMenuDetail();
});

$(document).keyup(function(e) {
  closeMenuDetail();
});


// go to Americanos!
$('body').on('click', '.SubcatMenu li, #menuLinkDrinks', function (){
  // var cardToScroll = ".MenuItem." + $(this).attr("class");
  $(".SubcatMenu li").removeClass("Active");
  $(this).addClass("Active");
  // var cardPosition = ($(cardToScroll).offset().top - 189)
  // if (document.documentElement.clientWidth < 800) {
  //   $('html,body').animate({
  //     scrollTop: cardPosition},
  //     200);
  //   $(".SubcatMenu li").removeClass("Active");
  //   $(this).addClass("Active");
  // };
  // if (document.documentElement.clientWidth > 800) {
  //   $('html,body').animate({
  //     scrollTop: $(cardToScroll).offset().top},
  //     200);
  //   $(".SubcatMenu li").removeClass("Active");
  //   $(this).addClass("Active");
  // }
});

//

// $('body').on('click', '.Search', function (){
//   // window.history.back();
//   var stateObj = { foo: "bar" };
//   history.pushState(stateObj, "page 2", "bar.html");
// });

// favorite heart!
$('body').on('click', '.heart-wrapper', function (){
  $(this).toggleClass("active")
});

//hammy 
$('body').on('click', '.Hammy', function (){
  $(this).toggleClass("Open");
  $('.HammyNav').toggleClass("Open")
  $('.NewGlobalNav').toggleClass("Open")
});




